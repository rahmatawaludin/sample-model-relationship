<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = ['email', 'password'];

    public function preference()
    {
        return $this->hasOne('App\Preference', 'account_id');
    }
}
