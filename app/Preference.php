<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{

    protected $fillable = ['user_id', 'country', 'currency', 'subscribe_mailing_list'];

    public function user()
    {
        return $this->belongsTo('App\User', 'account_id');
    }
}
