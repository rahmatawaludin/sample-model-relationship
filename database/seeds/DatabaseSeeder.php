<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserSeeder');
        $this->call('MovieSeeder');
        $this->call('StudentSeeder');
        $this->call('SongSeeder');
        $this->call('CommentSeeder');
        $this->call('PersonSeeder');
        $this->call('TagSeeder');
    }

}
